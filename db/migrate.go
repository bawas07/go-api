package db

import (
	"fmt"
	"os"

	"git.gits.id/RnD/WEB/golang-echo-starter/config"
	"git.gits.id/RnD/WEB/golang-echo-starter/modules/item"
)

// DBMigrate is function that automatically migrating table on database.
// Be aware that this migration won't delete or renaming existing table or field
func DBMigrate() {
	fmt.Println("==========================================================================")
	fmt.Println("Using database dialect: ", os.Getenv("DB_DIALECT"))
	fmt.Println("Connecting to ", os.Getenv("DB_HOST"), ":", os.Getenv("DB_PORT"))
	fmt.Println("==========================================================================")
	fmt.Println("Migrating Databases ...")
	db := config.DBConn()
	defer db.Close()
	fmt.Println("Migrating Item ...")
	db.AutoMigrate(&item.Item{})
	fmt.Println("Migration Databases Done")
}
