package item

import (
	"git.gits.id/RnD/WEB/golang-echo-starter/config"
	"github.com/jinzhu/gorm"
)

type ItemRepository struct {
	query itemQueryInterface
}

func GetRepo(conn *gorm.DB) ItemRepositoryInterface {
	var query itemQueryInterface
	if conn == nil {
		query = itemQuery{db: config.DBConn()}
	} else {
		query = itemQuery{db: conn}
	}
	var repo ItemRepository = ItemRepository{query: query}
	return repo
}

func (repo ItemRepository) GetAll(resItems *[]Item) error {
	err := repo.query.findAll(resItems)
	if err != nil {
		return err
	}
	return nil
}

func (repo ItemRepository) GetOne(resItem *Item, id string) error {
	err := repo.query.findById(resItem, id)
	if err != nil {
		return err
	}
	return nil
}

func (repo ItemRepository) CreateItem(item *Item) error {
	err := repo.query.create(item)
	if err != nil {
		return err
	}
	return nil
}

func (repo ItemRepository) DeleteById(resItem *Item, id string) error {
	return nil
}

func (repo ItemRepository) UpdateById(item *Item, id int) error {
	return nil
}
