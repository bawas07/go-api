package item

import (
	"net/http"

	"git.gits.id/RnD/WEB/golang-echo-starter/renderings"
	"github.com/labstack/echo"
)

func getItemsRouteHandler(c echo.Context) error {
	var itemRepo = GetRepo(nil)
	var items []Item
	if err := itemRepo.GetAll(&items); err != nil {
		return err
	}
	res := renderings.Response{
		Code:    http.StatusOK,
		Success: true,
		Message: "Data retrieved",
		Data:    items,
	}
	return c.JSON(http.StatusOK, res)
}

func getOneItemRouteHandler(c echo.Context) error {
	var itemRepo = GetRepo(nil)
	var item Item
	err := itemRepo.GetOne(&item, c.Param("id"))
	if err != nil {
		return err
	}
	res := renderings.Response{
		Code:    http.StatusOK,
		Success: true,
		Message: "Data retrieved",
		Data:    item,
	}
	return c.JSON(http.StatusOK, res)
}

func storeItem(c echo.Context) error {
	var itemRepo = GetRepo(nil)
	// var itemData Item
	itemData := new(Item)
	var err error
	if err = c.Bind(itemData); err != nil {
		return err
	}
	err = itemRepo.CreateItem(itemData)
	if err != nil {
		return err
	}
	res := renderings.Response{
		Code:    http.StatusOK,
		Success: true,
		Message: "Data retrieved",
		Data:    itemData,
	}
	return c.JSON(http.StatusOK, res)
}
