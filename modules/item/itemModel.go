package item

import "github.com/jinzhu/gorm"

type (
	Item struct {
		gorm.Model
		SKU        string `gorm:"not null"`
		NamaBarang string `gorm:"not null;"`
	}

	handle struct {
		db *Item
	}
)
