package item

import (
	"github.com/jinzhu/gorm"
)

type itemQuery struct {
	db *gorm.DB
}

func (q itemQuery) findAll(resItems *[]Item) error {
	db := q.db
	defer db.Close()
	var items []Item
	if err := db.Find(&items).Error; err != nil {
		return err
	}
	*resItems = items
	return nil
}

func (q itemQuery) findById(resItem *Item, id string) error {
	db := q.db
	defer db.Close()
	var item Item
	if err := db.First(&item, id).Error; err != nil {
		return err
	}
	*resItem = item
	return nil
}

func (q itemQuery) create(item *Item) error {
	db := q.db
	defer db.Close()
	// var item Item
	if err := db.Create(item).Error; err != nil {
		return err
	}
	// *resItem = item

	return nil
}

func (q itemQuery) deleteById(resItem *Item, id string) error {
	return nil
}

func (q itemQuery) updateById(item *Item, id int) error {
	return nil
}
