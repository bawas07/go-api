package item

import (
	"github.com/labstack/echo"
)

// var h *handler

// Router is a group or route declaration in module user
func Router(e *echo.Group) {
	e.GET("/", getItemsRouteHandler)
	e.GET("/:id", getOneItemRouteHandler)
	e.POST("/", storeItem)
	// e.PUT("/:id", updateUser)
	// e.DELETE("/:id", deleteOneUser)
}
