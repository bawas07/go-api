package item

type ItemRepositoryInterface interface {
	GetAll(resItems *[]Item) error
	GetOne(resItem *Item, id string) error
	CreateItem(item *Item) error
	DeleteById(resItem *Item, id string) error
	UpdateById(item *Item, id int) error
}

type itemQueryInterface interface {
	findAll(resItems *[]Item) error
	findById(resItem *Item, id string) error
	create(item *Item) error
	deleteById(resItem *Item, id string) error
	updateById(item *Item, id int) error
}
