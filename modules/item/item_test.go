package item

import (
	"fmt"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func TestGetAllItems(t *testing.T) {

	if err := godotenv.Load(); err != nil {
		fmt.Println("File .env not found, reading configuration from ENV")
	}
	var gotItem Item
	repo := GetRepo(nil)
	repo.GetOne(&gotItem, "1")
	msg := fmt.Sprintf("SKU must be gits-123456, got %v", gotItem.SKU)
	var want string = "gits-123456"
	assert.Equal(t, want, gotItem.SKU, msg)
}
