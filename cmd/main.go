package main

import (
	"fmt"
	"net/http"
	"time"

	"git.gits.id/RnD/WEB/golang-echo-starter/db"
	"git.gits.id/RnD/WEB/golang-echo-starter/routes"

	"git.gits.id/RnD/WEB/golang-echo-starter/renderings"
	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	log "github.com/sirupsen/logrus"
)

func makeLogEntry(c echo.Context) *log.Entry {
	if c == nil {
		return log.WithFields(log.Fields{
			"at": time.Now().Format("2006-01-02 15:04:05"),
		})
	}

	return log.WithFields(log.Fields{
		"at":     time.Now().Format("2006-01-02 15:04:05"),
		"method": c.Request().Method,
		"uri":    c.Request().URL.String(),
		"ip":     c.Request().RemoteAddr,
	})
}

func middlewareLogging(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		makeLogEntry(c).Info("incoming request")
		return next(c)
	}
}

func errorHandler(err error, c echo.Context) {
	report, ok := err.(*echo.HTTPError)
	if ok {
		report.Message = fmt.Sprintf("http error %d - %v", report.Code, report.Message)
	} else {
		report = echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	makeLogEntry(c).Error(report.Message)
	res := renderings.Response{
		Code:    report.Code,
		Success: false,
		Message: report.Message.(string),
		Data:    nil,
	}
	c.JSON(report.Code, res)
}

func main() {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	db.DBMigrate()

	e := echo.New()
	//time:${time_rfc3339_nano}, remote_ip:${remote_ip}, user_agent:${user_agent}, host:${host},
	//method:${method}, uri:${uri}, status:${status}, error:${error}

	// Middleware
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		//Format: "method=${method}, uri=${uri}, status=${status}\n",
		Format: "time:${time_rfc3339_nano}, remote_ip:${remote_ip}, user_agent:${user_agent}, host:${host}, method:${method}, uri:${uri}, status:${status}, error:${error}\n",
	}))
	//e.Use(middlewareLogging)
	e.HTTPErrorHandler = errorHandler
	e.Use(middleware.Recover())
	//e.SetDebug(true)

	// CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{
			echo.GET,
			echo.HEAD,
			echo.PUT,
			echo.PATCH,
			echo.POST,
			echo.DELETE,
		},
	}))

	// // Routes
	// e.GET("/", func (c echo.Context) error {
	// 	return c.String(http.StatusOK, "Hello World!")
	// })

	v1 := e.Group("/api")
	routes.RouteAPI(v1)
	// userRouter := v1.Group("/user")
	// user.Router(userRouter)

	// Start server
	e.Logger.Fatal(e.Start(":5000"))
}
