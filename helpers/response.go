package helpers

import (
	"net/http"

	"git.gits.id/RnD/WEB/golang-echo-starter/renderings"
)

func ResponseSuccess(data interface{}, msg string) renderings.Response {
	var message string = "Data retrieved"
	if msg != "" {
		message = msg
	}
	return renderings.Response{
		Code:    http.StatusOK,
		Success: true,
		Message: message,
		Data:    data,
	}
}
