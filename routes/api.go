package routes

import (
	"git.gits.id/RnD/WEB/golang-echo-starter/modules/item"
	"github.com/labstack/echo"
)

func RouteAPI(api *echo.Group) {
	v1 := api.Group("/v1")

	itemRouter := v1.Group("/items")
	item.Router(itemRouter)
}
